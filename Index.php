<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Xbox 360 Minecraft</title> 
</head>
<body>
	
	<h1>Xbox 360 Minecraft Couch Coop</h1>
	<table>
		  <tr>
    <td><a href="/Assignment%20one/Public_Webpages/noobs.html">Introducing new players</a></td>
    <td><a href="/Assignment%20one/Public_Webpages/Ravine.html">Ravine</a></td>
    <td><a href="/Assignment%20one/Public_Webpages/floating.html">Floating Buildings</a></td>
  </tr>
	</table>
	<p> Minecraft on the PC does come with it's benefits like how it's way better updated and you can use mods. I'm not bashing playing multiplayer either you can have fun playing online too. But with all the talk about Minecraft I'm surprised there isn't more out there about the benefits of playing in couch coop. Just to name a few: </p>
	<h2>Pseudo Socialism</h2>
	<p>When you’re talking about applying Socialism to a country it has it's disadvantages, but when you apply it to up to four people and keep in mind that this is the distribution of virtual resources it isn't that big of a deal. Player edicate in any multiplayer involves some elements of Socialism like if you horde things that you don't need or if you cheat to get unlimited resources you’re a jerk. The benefit of playing on the couch if you are sitting next someone who isn't a team player you can physically make them a team player. It can even be a team effort. </p>
	<h3>Anonymity of the Internet</h3>
	<p>You know when you play online and you meet the kid that just learned the n-word and is using it in every possible sentence like he is checking to make sure he still knows how to pronounce it correctly or the mistaken young gentleman than that is under the impression that a certain homophobic slur is actually an adjective that is interchangeable with any other adjective in the English language? They still exist and it is possible to play with them in your living room together, but they're less chatty when they know they can be accountable for what they say. It's not as appealing to inform someone about their mother's sexual exploits when she is in the other room. </p>
	<h4>AFK Drama</h4>
	<p> It's not as much of an issue. With practice it isn't that big of a deal you just move them close to a bed and have them sleep while you work. Then when your buddy comes back from the bathroom or where ever they can get back to playing. No big deal.</p>
</body>
